GQL/CL
======

GQL/CL is a Constraint Language for the Graph Query Language. This repository contains the specification and test suite for the language.

Development happens in the dev/1.0 branch.
